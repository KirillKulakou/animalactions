### What you'll need ###

Install dotnet 5 from link: https://dotnet.microsoft.com/download/dotnet/5.0

### How to run ###

1. Cd into project direcotry 

2. Execute `dotnet build`

3. Execute `dotnet run`

### Sample Output ###

```
Mega Duck!
I am animal that can eat!
I can swim because I am a duck!
I can fly because I am a duck!
Quak!
============================================
Mega Penguin!
I am animal that can eat!
I can swim because I am a Penguin
Braying sound!
============================================
Mega Parrot!
I am animal that can eat!
I am parrot I can fly very low!
Braying sound!
============================================
Mega Pterodactyl!
I don't eat because I don't exist.
I could fly when I was alive!
I dont know what this sounds like :(
```
