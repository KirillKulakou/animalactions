﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions
{
    internal static class AnimalActions
    {
        internal static string Eat(this IAnimal self)
        {
            return self.MyFood;
        }

        internal static string Swim(this ISwim self)
        {
            return self.Swimming;
        }

        internal static string Fly(this IFly self)
        {
            return self.Flying;
        }

        internal static string GetAnimalSound(this IAnimal self)
        {
            return self.Sound;
        }
    }
}

//Method #1: Eat
//Method #2: Swim
//Method #3: Fly
//Method #4: GetAnimalSound