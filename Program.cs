﻿using System;
using AnimalActions.Animals;

namespace AnimalActions
{
    class Program
    {
        static void Main(string[] args)
        {
            var duck = new Duck();
            Console.WriteLine("Mega Duck!");
            Console.WriteLine(duck.Eat());
            Console.WriteLine(duck.Swim());
            Console.WriteLine(duck.Fly());
            Console.WriteLine(duck.GetAnimalSound());

            Console.WriteLine("============================================");

            var penguin = new Penguin();
            Console.WriteLine("Mega Penguin!");
            Console.WriteLine(penguin.Eat());
            Console.WriteLine(penguin.Swim());
            Console.WriteLine(penguin.GetAnimalSound());

            Console.WriteLine("============================================");
            
            var parrot = new Parrot();
            Console.WriteLine($"Mega {nameof(Parrot)}!");
            Console.WriteLine(parrot.Eat());
            Console.WriteLine(parrot.Fly());
            Console.WriteLine(penguin.GetAnimalSound());

            Console.WriteLine("============================================");

            var pterodactyl = new Pterodactyl();
            Console.WriteLine($"Mega {nameof(Pterodactyl)}!");
            Console.WriteLine(pterodactyl.Eat());
            Console.WriteLine(pterodactyl.Fly());
            Console.WriteLine(pterodactyl.GetAnimalSound());

            Console.ReadLine();
        }
    }
}