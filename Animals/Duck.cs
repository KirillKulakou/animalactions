﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions.Animals
{
    public class Duck : Animal, ISwim, IFly
    {
        public string Swimming => "I can swim because I am a duck!";
        public string Flying => "I can fly because I am a duck!";
        public override string Sound => "Quak!";
    }
}