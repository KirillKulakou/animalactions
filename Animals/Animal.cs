﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions.Animals
{
    public abstract class Animal : IAnimal
    {
        public virtual string MyFood => "I am animal that can eat!";
        public abstract string Sound { get; }
    }
}