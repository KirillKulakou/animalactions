﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions.Animals
{
    public class Pterodactyl : Animal, IFly
    {
        public override string MyFood => "I don't eat because I don't exist.";
        public string Flying => "I could fly when I was alive!";
        public override string Sound => "I dont know what this sounds like :(";
    }
}