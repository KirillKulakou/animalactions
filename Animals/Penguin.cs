﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions.Animals
{
    public class Penguin : Animal, ISwim
    {
        public string Swimming => $"I can swim because I am a {nameof(Penguin)}";
        public override string Sound => "Braying sound!";
    }
}