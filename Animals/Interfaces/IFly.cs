﻿namespace AnimalActions.Animals.Interfaces
{
    public interface IFly
    {
        public string Flying { get; }
    }
}