﻿namespace AnimalActions.Animals.Interfaces
{
    public interface ISwim
    {
        public string Swimming { get; }
    }
}