﻿namespace AnimalActions.Animals.Interfaces
{
    public interface IAnimal
    {
        public string MyFood { get; }
        public string Sound { get; }
    }
}