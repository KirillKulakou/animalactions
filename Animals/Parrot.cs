﻿using AnimalActions.Animals.Interfaces;

namespace AnimalActions.Animals
{
    public class Parrot: Animal, IFly
    {
        public string Flying => "I am parrot I can fly very low!";

        public override string Sound => "Whatever my owner taught me!";

    }
}